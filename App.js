import React from 'react';
import { createStackNavigator,createAppContainer} from 'react-navigation';
import ListScreen from './screens/ListScreen';
import chordScreen from './screens/chordScreen';
import AkinKaNaLang from './screens/Akin_ka_na_lang';
import Bulong from './screens/Bulong';
import CountingStars from './screens/Counting_Stars';
import DahilSayo from './screens/Dahil_sayo';
import Ewan from './screens/Ewan';
import Fresheyes from './screens/Fresh_eyes';
import Girlslikeyou from './screens/Girls_like_you';
import Happier from './screens/Happier';
import IDGAF from './screens/IDGAF';
import Justthewayyouare from './screens/Just_the_way_you_are';
import Kahitayawmona from './screens/Kahit_ayaw_mo_na';
import Lovestory from './screens/Love_story';



const RootStack = createStackNavigator(
  { 
    List: ListScreen,
    chord: chordScreen,
    one: AkinKaNaLang,
    two: Bulong,
    three: CountingStars,
    four: DahilSayo,
    five: Ewan,
    six: Fresheyes,
    seven: Girlslikeyou,
    eight: Happier,
    nine: IDGAF,
    ten: Justthewayyouare,
    eleven: Kahitayawmona,
    twelve: Lovestory,
  },
  {
    initialRouteName: 'List',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}