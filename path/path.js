
var path = [	
			
			require('../assets/chords/A.png'), 		//0 - A
			require('../assets/chords/B.png'),		//1 - B
			require('../assets/chords/C.png'),		//2 - C
			require('../assets/chords/D.png'),		//3 - D
			require('../assets/chords/E.png'),		//4 - E
			require('../assets/chords/F.png'),		//5 - F
			require('../assets/chords/G.png'),		//6 - G
			require('../assets/chords/Am.png'),		//7 - Am
			require('../assets/chords/Bm.png'),		//8 - Bm
			require('../assets/chords/Cm.png'),		//9 - Cm
			require('../assets/chords/Dm.png'),		//10 - Dm
			require('../assets/chords/Em.png'),		//11 - Em
			require('../assets/chords/Fm.png'),		//12 - Fm	
			require('../assets/chords/Gm.png'),		//13 - Gm
			require('../assets/chords/A.png'),		//14 - A
			require('../assets/chords/Csm.png'),	//15 - C#m
			require('../assets/chords/Em_Ds.png'),	//16 - Em/D#
			require('../assets/chords/Em_D.png'),	//17 - Em/D
			require('../assets/chords/B7.png'),		//18 - B7
			require('../assets/chords/D_Fs.png'),	//19 - D/F#
			require('../assets/chords/B_C.png'),	//20 - B/C
			require('../assets/chords/Am7.png'),	//21 - Am7
			// require('../assets/chords/F#m.png'),	//22 - F#m X

		 ]

export {path};