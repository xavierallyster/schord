import React, {Component} from 'react';
import {Text, ScrollView, StyleSheet} from 'react-native';


export default class LyricsScreen extends Component { 

render() {
    const { navigation } = this.props;

    const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <ScrollView >
      <Text>
      FRESH EYES {"\n"}
      Artist: Andy Grammer{"\n"}
      {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>               Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     I got these fresh eyes, never seen you before like this{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     My God, you're beautiful{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>               Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     It's like the first time when we open the door{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     Before we got used to usual{"\n"}
     {"\n"}
     [Pre-Chorus]{"\n"}
     {"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     It might seem superficial,{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     stereotypical, man{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     You dress up just a little{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>         Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     and I'm like, "Oohhh, damn"{"\n"}
     {"\n"}
     [Chorus]{"\n"}
     {"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     So suddenly I'm in love with a stranger{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     I can't believe that she's mine{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     Now all I see is you{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     Now all I see is you{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     So suddenly I'm in love with a stranger{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     I can't believe that she's mine{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     Now all I see is you{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
     Now all I see is you{"\n"}


      </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ll: {
    color: 'green' 
  }
})

