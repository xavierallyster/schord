import React, {Component} from 'react';
import {Text, ScrollView, StyleSheet} from 'react-native';


export default class LyricsScreen extends Component { 

render() {
    const { navigation } = this.props;

    const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <ScrollView >
      <Text>
      Dahil Sa'yo{"\n"}
      Artist: Inigo Pascual{"\n"}
      {"\n"}
      [Verse 1] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>                                          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text> {"\n"}  
      Araw araw ikaw ang gusto kong kasama{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                                                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      Buhay ko’y kumpleto na tuwing nandidito ka{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>                                                                         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text> {"\n"}  
      Sa tabi ko o aking giliw di pa din ako makapaniwala{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                                                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      Na ang dati kong pangarap ay katotohanan na{"\n"}
      {"\n"}
      [Pre-Chorus] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text> {"\n"}  
      Ikaw ang tanging inspirasyon{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      At basta’t nandito ka ako’y liligaya{"\n"}
      {"\n"}
      [Chorus] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Dahil sa’yo ako’y matapang{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>{"\n"}
      Dahil sa’yo ako’y lalaban{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      Para sa’yo pagmamahal na walang katapusan{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Dahil sa’yo merong pangarap{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>{"\n"}
      Pagmamahal ko sayo’y tapat{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      Para sa’yo pagmamahal na higit pa sa sapat{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text> {"\n"}  
      Gagawin ko ang lahat para lang sa’yo sinta{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      At basta’t nandito ka ako’y liligaya{"\n"}
      {"\n"}
      [Verse 2] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>                                          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text> {"\n"}  
      Minuminuto naghihintay ng tawag mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                                                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      Marinig lang boses mo masaya’t kuntento na ko{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>                                                                         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text> {"\n"}  
      Wala ng iba pang hahanapin basta’t ikaw ang aking kapiling{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                                                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      Lahat magagawa dahil kasama ka{"\n"}
      {"\n"}
      [Pre-Chorus] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text> {"\n"}  
      Ikaw ang tanging inspirasyon{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      At basta’t nandito ka ako’y liligaya{"\n"}
      {"\n"}
      [Chorus] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Dahil sa’yo ako’y matapang{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>{"\n"}
      Dahil sa’yo ako’y lalaban{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      Para sa’yo pagmamahal na walang katapusan{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Dahil sa’yo merong pangarap{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>{"\n"}
      Pagmamahal ko sayo’y tapat{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      Para sa’yo pagmamahal na higit pa sa sapat{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text> {"\n"}  
      Gagawin ko ang lahat para lang sa’yo sinta{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      At basta’t nandito ka ako’y liligaya{"\n"}
       {"\n"}
      [Bridge] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}>                      C </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text> {"\n"}  
      Ipinagdarasal ko ng sobra na sana’y tanggapin mo aking inaalay{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}>       Dm </Text>{"\n"}
      Na pasasalamat sa pagliliwanag ng buhay kong ito{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"}
      Na dati rati’y di ganito na kay ligaya{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
     Oh tanggapin ang regalo{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>{"\n"}
     Oh mga rosas at choco{"\n"}
     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      Liliwanag din ang buhay mo pag nilabas ko na ang puso ko{"\n"}
      {"\n"}
      [Pre-Chorus] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text> {"\n"}  
      Ikaw ang tanging inspirasyon{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      At basta’t nandito ka ako’y liligaya{"\n"}
      {"\n"}
      [Chorus] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Dahil sa’yo ako’y matapang{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>{"\n"}
      Dahil sa’yo ako’y lalaban{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      Para sa’yo pagmamahal na walang katapusan{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Dahil sa’yo merong pangarap{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>{"\n"}
      Pagmamahal ko sayo’y tapat{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      Para sa’yo pagmamahal na higit pa sa sapat{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '10',})}> Dm </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text> {"\n"}  
      Gagawin ko ang lahat para lang sa’yo sinta{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text> {"\n"}  
      At basta’t nandito ka ako’y liligaya{"\n"}

      </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ll: {
    color: 'green' 
  }
})

