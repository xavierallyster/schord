import React, {Component} from 'react';
import {Text, ScrollView, StyleSheet} from 'react-native';


export default class LyricsScreen extends Component { 

render() {
    const { navigation } = this.props;

    const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <ScrollView >
      <Text>
      JUST THE WAY YOU ARE {"\n"}
      Artist: Bruno Mars{"\n"}
      {"\n"}
      [Intro: ]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>D </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>{"\n"} 
      Ah, ah ah ah{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>         G </Text>                  <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      Ah ah ah ah, ah ah ah{"\n"}
      {"\n"}
      [Verse]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>D </Text>{"\n"} 
      Oh her eyes, her eyes, make the stars look like they're not shining{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}>Bm </Text>{"\n"} 
      Her hair, her hair, falls perfectly without her trying{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>         G </Text>                                                     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      She's so beautiful, and I tell her every day{"\n"}
      {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>D </Text>{"\n"} 
      Yeah, I know, I know when I compliment her she won't believe me{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}>Bm </Text>{"\n"} 
      And it's so, it's so sad to think that she don't see what I see{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>         G </Text>                                                        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      But every time she asks me do I look okay, I say{"\n"}
      {"\n"}
      [Chorus]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>                         D </Text>                               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>{"\n"} 
      When I see your face, there's not a thing that I would change{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>                       G </Text>                                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      Cause you're amazing, just the way you are{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>                         D </Text>                        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>{"\n"} 
      And when you smile, the whole world stops and stares for a while{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>                        G </Text>                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      Cause girl you're amazing, just the way you are, (yeah){"\n"}
      {"\n"}
      [Verse]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>D </Text>{"\n"} 
      Her lips, her lips, I could kiss them all day if she'd let me{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}>Bm </Text>{"\n"} 
      Her laugh, her laugh, she hates but I think it's so sexy{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>         G </Text>                                                     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      She's so beautiful, and I tell her every day{"\n"}
      {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>D </Text>{"\n"} 
      Oh, you know, you know, you know I'd never ask you to change{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}>Bm </Text>{"\n"} 
      If perfect is what you're searching for, then just stay the same{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>         G </Text>                                                                  <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      So, don't even bother asking if you look okay, you know I'll say{"\n"}
      {"\n"}
      [Chorus]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>                         D </Text>                               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>{"\n"} 
      When I see your face, there's not a thing that I would change{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>                       G </Text>                                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      Cause you're amazing, just the way you are{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>                         D </Text>                        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>{"\n"} 
      And when you smile, the whole world stops and stares for a while{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>                        G </Text>                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      Cause girl you're amazing, just the way you are, (yeah){"\n"}



      </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ll: {
    color: 'green' 
  }
})

