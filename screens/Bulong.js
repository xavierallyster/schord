import React, {Component} from 'react';
import {Text, ScrollView, StyleSheet} from 'react-native';


export default class LyricsScreen extends Component { 

render() {
    const { navigation } = this.props;

    const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <ScrollView >
      <Text>
      BULONG{"\n"}
      Artist: December Avenue{"\n"}
      {"\n"}
      Intro: {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Am7 </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> G </Text>{"\n"} 
      {"\n"}
      [Verse 1]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>          F </Text>                    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Hindi masabi ang nararamdaman{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>               F </Text>                           <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Di makalapit, sadyang nanginginig nalang{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>          F </Text>                                          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Mga kamay na sabik sa piling mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>                          F </Text>                                                     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>{"\n"} 
      Ang iyong matang walang mintis sa pagtigil ng aking{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>      G </Text>{"\n"}
      mundo{"\n"}
      {"\n"}
      [Chorus]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>          F </Text>                             <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Ako'y alipin ng pag-ibig mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>               F </Text>                                      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text> <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Handang ibigin ang 'isang tulad mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>                        F </Text>                                            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Hangga't ang puso mo'y sa akin lang hindi ka na malilinlang{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                                                            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"}
      Ikaw ang ilaw sa dilim at ang liwanag ng mga bituin{"\n"}   
       {"\n"}
      [Verse 2]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>          F </Text>                    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Hindi mapakali, hanggang tingin nalang{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>               F </Text>                           <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Bumubulong sa iyong tabi, sadyang walang makapantay{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>          F </Text>                                  <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Sa kagandahang inuukit mo sa isip ko{"\n"}
      {"\n"}
      [Chorus]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>          F </Text>                             <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Ako'y alipin ng pag-ibig mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>               F </Text>                                      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text> <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Handang ibigin ang 'isang tulad mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>                        F </Text>                                            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Hangga't ang puso mo'y sa akin lang hindi ka na malilinlang{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                                                            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"}
      Ikaw ang ilaw sa dilim at ang liwanag ng mga bituin{"\n"}   
      [Bridge] {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Am7 </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      {"\n"}
      [Chorus]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>          F </Text>                             <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Ako'y alipin ng pag-ibig mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>               F </Text>                                      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text> <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Handang ibigin ang 'isang tulad mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>                        F </Text>                                            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Hangga't ang puso mo'y sa akin lang hindi ka na malilinlang{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                                                            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '21',})}> Am7 </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"}
      Ikaw ang ilaw sa dilim at ang liwanag ng mga bituin{"\n"}   
      </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ll: {
    color: 'green' 
  }
})

