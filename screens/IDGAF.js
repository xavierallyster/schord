import React, {Component} from 'react';
import {Text, ScrollView, StyleSheet} from 'react-native';


export default class LyricsScreen extends Component { 

render() {
    const { navigation } = this.props;

    const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <ScrollView >
      <Text>
      FRESH EYES {"\n"}
      Artist: Andy Grammer{"\n"}
      {"\n"}
      [Intro:]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
      {"\n"}
      {"\n"}
      [Verse]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      You call me all friendly{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}>                                 C </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>{"\n"} 
      Tellin' me how much you miss me{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>                      D </Text>                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"} 
      That's funny, I guess you've heard my songs{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>        Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      Well, I'm too busy for your business{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>        G </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"} 
      Go find a girl who wants to listen{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>Em </Text>                   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>                    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
      'Cause if you think I was born yesterday, you have got me wrong{"\n"}
      {"\n"}
      [Pre-Chorus]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>          Em </Text>{"\n"} 
      So I cut you off{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      I don't need your love{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"} 
      'Cause I already cried enough{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>          Em </Text>{"\n"} 
      I've been done{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>                      D </Text>                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"} 
      I've been movin' on since we said goodbye{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>          Em </Text>{"\n"} 
      So I cut you off{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      I don't need your love{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"} 
      'Cause I already cried enough{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>          Em </Text>{"\n"} 
      So you can try all you want{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>                      D </Text>                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"} 
      Your time is up, I'll tell you why{"\n"}
      {"\n"}
      [Chorus]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>      Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
      You say you're sorry, but it's too late now{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>    Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
      So save it, get gone, shut up{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>      Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
      'Cause if you think I care about you now{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}>          Em </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
      Well, boy, I don't give a fuck{"\n"}





      </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ll: {
    color: 'green' 
  }
})

