

import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
} from "react-native";
import { List, ListItem, SearchBar } from "react-native-elements";
// import { getUsers } from "./api/index";
import _ from 'lodash';
import songs from '../data/songs.json'

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      error: null,
      query: "", 
     fullData: [],
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {
    this.setState({ 
      loading: false, 
      fullData: songs,
      data: songs,
    });

  
  };

  handleSearch = (text) => {


    const contains = ({ Title, Artist }, query) => {
      if (Title.includes(query) || Artist.includes(query)) {
        return true;
      }

      return false;
    };
    
    const formatQuery = text;
    const data=_.filter(this.state.fullData, songs => {
        return contains(songs, formatQuery);
    });
    this.setState({ query: formatQuery, data });
  };
 
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#CED0CE",
          marginLeft: "5%"
        }}
      />
    );
  };

  renderHeader = () => {
    return <SearchBar placeholder="Type Here..." lightTheme round onChangeText ={this.handleSearch} />;
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
        <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
          <FlatList
            data={this.state.data}
            renderItem={({ item }) => (
              <ListItem
                onPress={() => {
                 /* 1. Navigate to the Details route with params */
                   this.props.navigation.navigate(item.Key, {
                     otherParam: item.Title,
                 });
               }}
                title={`${item.Title}`}
                subtitle={item.Artist}
                
                containerStyle={{ borderBottomWidth: 0 }}
              />
            )}
            keyExtractor={item => item.Title}
            ItemSeparatorComponent={this.renderSeparator}
            ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
          />
        </List>
    );
  }
}

export default App;