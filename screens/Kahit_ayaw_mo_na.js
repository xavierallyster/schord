import React, {Component} from 'react';
import {Text, ScrollView, StyleSheet} from 'react-native';


export default class LyricsScreen extends Component { 

render() {
    const { navigation } = this.props;

    const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <ScrollView >
      <Text>
      KAHIT AYAW MO NA {"\n"}
      Artist: This Band{"\n"}
      {"\n"}
      [Intro:]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>D </Text> {"\n"} 
      {"\n"}
      {"\n"}
      [Verse]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>                        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>{"\n"} 
      Kahit ikaw ay magalit{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>           G </Text>{"\n"} 
      Sayo lang lalapit{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>           D </Text>{"\n"} 
      Sayo lang aawit{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>{"\n"} 
      Kahit na ikaw ay nagbago na{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>           G </Text>{"\n"} 
      Iibigin pa rin kita{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>           D </Text>{"\n"} 
      Kahit ayaw mo na{"\n"}
      {"\n"}
      [Pre-chorus]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>       G </Text>                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>                   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      Tatakbo, tatalon, sisigaw ang pangalan mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>     G </Text>                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      Iisipin na lang panaginip lahat ng ito{"\n"}
      {"\n"}
      {"\n"}
      [Chorus]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>                  G </Text>                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"} 
      O, bakit ba kailangan pang umalis{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>                           D </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text>     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>{"\n"} 
      Pakiusap lang na wag ka nang lumihis{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>                     G </Text>                              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"} 
      Tayoy mag usap teka lang ika'y huminto{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}>                           D </Text>                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text>     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>{"\n"} 
      Wag mo kong iwan aayusin natin 'to{"\n"}
      </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ll: {
    color: 'green' 
  }
})

