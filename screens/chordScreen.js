import React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';
import {path} from '../path/path';



export default class chordScreen extends React.Component {
  render () {

    const { navigation } = this.props;

    const chordz = navigation.getParam('chordz', 'some default value');


    return(
        <View>
          <Image
            style={styles.container}
            source={path[chordz]}
          />
        </View>
      );
  }
}

const styles = StyleSheet.create ({
  container: {
    alignSelf: 'center',
    margin: 10,
    height: 400,
    width: 350,
    resizeMode: "stretch",
  }
})