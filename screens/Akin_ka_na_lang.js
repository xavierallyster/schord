import React, {Component} from 'react';
import {Text, ScrollView, StyleSheet} from 'react-native';


export default class LyricsScreen extends Component { 

render() {
    const { navigation } = this.props;

    const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <ScrollView >
      <Text>
      AKIN KA NA LANG {"\n"}
      Artist: Itchyworms{"\n"}
      Album: Noontime Show{"\n"}
      {"\n"}
      Intro: {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text> 3x{"\n"} 
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"} 
      {"\n"}
      [Verse 1]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      'Wag kang maniwala d'yan{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      'Di ka n'ya mahal talaga{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      Sayang lang ang buhay mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>               G </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      kung mapupunta ka lang sa kanya{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '16',})}> Em/D# </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '17',})}> Em/D </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text>{"\n"}
      '      Iiwanan ka lang n'yan, mag-ingat ka{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '9',})}> Cm </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      Dagdag ka lamang sa milyun-milyong babae n'ya{"\n"}
      {"\n"}
      [Chorus]{"\n"}
      {"\n"}
       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"}
      ingatan ko ang puso mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}
      {"\n"}
      [interlude]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      {"\n"}
      [Verse 2]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
       Di naman ako bolero{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      Katulad ng ibang tao{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      Sayang lang ang buhay mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      Ang totoo'y pag nandyan ka{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>               G </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      Medyo nabubulol pa nga ako{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '16',})}> Em/D# </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '17',})}> Em/D </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '9',})}> C#m </Text>{"\n"}
      Malangis lang ang dila nyan{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text>{"\n"}
      Wag kang padala
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>{"\n"}
      Dahan-dahan ka lang{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '9',})}> Cm </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      Baka pati ika'y mabiktima{"\n"}
      Wag naman sana{"\n"}
      {"\n"}
      [Chorus]{"\n"}
      {"\n"}
       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"}
      ingatan ko ang puso mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}
      {"\n"}
      [Bridge]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>                   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>            <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '19',})}> D/F# </Text>{"\n"}
      Di naman sa sinisiraan ko ang pangit na yan{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '11',})}> Em </Text>                   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>          <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '20',})}> B/C </Text>{"\n"}
      Wag ka dapat sa 'kin magduda{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}>                  C </Text>                   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '18',})}> B7 </Text>{"\n"}
     Hinding-hindi kita pababayaan{"\n"}
     {"\n"}
      [Chorus]{"\n"}
      {"\n"}
       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"}
      ingatan ko ang puso mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}
      {"\n"}
      [Chorus]{"\n"}
      {"\n"}
       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"}
      ingatan ko ang puso mo{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> Bm </Text>               <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '3',})}> D </Text>{"\n"}
      Akin ka na lang (akin ka na lang){"\n"}

      </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ll: {
    color: 'green' 
  }
})

