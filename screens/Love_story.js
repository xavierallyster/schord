import React, {Component} from 'react';
import {Text, ScrollView, StyleSheet} from 'react-native';


export default class LyricsScreen extends Component { 

render() {
    const { navigation } = this.props;

    const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <ScrollView >
      <Text>
      LOVE STORY {"\n"}
      Artist: Taylor Swift{"\n"}
      {"\n"}
      [Intro]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>{"\n"} 
      {"\n"}
      {"\n"}
      [Verse]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"} 
      We were both young when I first saw you{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>{"\n"} 
      I close my eyes and the flashback starts{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}>   Am </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>  F </Text>{"\n"} 
      I'm standing there on a balcony in summer air{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"} 
      See the lights, see the party, the ball gowns{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>{"\n"} 
      See you make your way through the crowd{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}>           Am </Text>{"\n"} 
      And say, "Hello"{"\n"}  
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>       F </Text>{"\n"}
      Little did I know{"\n"}
      {"\n"}
      [Pre-chorus]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                  <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      That you were Romeo, you were throwing pebbles{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>                      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"} 
      And my daddy said, "Stay away from Juliet"{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>{"\n"} 
      And I was crying on the staircase{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>                             <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>{"\n"} 
      Begging you please don't go, and I said{"\n"}
      {"\n"}
      [Chorus]{"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"} 
      Romeo take me somewhere we can be alone{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}>    G </Text>{"\n"} 
      I'll be waiting all there's left to do is run{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}>          Am </Text>{"\n"} 
      You'll be the prince and I'll be the princess{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '6',})}> G </Text>                        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text>{"\n"} 
      It's a love story baby just say "yes"{"\n"}




      </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ll: {
    color: 'green' 
  }
})

