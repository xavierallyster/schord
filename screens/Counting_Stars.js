import React, {Component} from 'react';
import {Text, ScrollView, StyleSheet} from 'react-native';


export default class LyricsScreen extends Component { 

render() {
    const { navigation } = this.props;

    const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <ScrollView >
      <Text>
      Counting Stars{"\n"}
      Artist: One Republic{"\n"}
      {"\n"}
      Intro: {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>-<Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      {"\n"}
      {"\n"}
      [Chorus] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text>                     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>{"\n"}
      Lately, I've been, I've been losing sleep{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>                                           <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      Dreaming about the things that we could be{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}>        C#m </Text>                     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>{"\n"}
      Said, no more counting dollars{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>                                           <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      We'll be counting stars, yeah we'll be counting stars{"\n"}
      {"\n"}
      [Verse 1] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}>                    C#m </Text>{"\n"}  
      I see this life like a swinging vine{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>{"\n"}  
      Swing my heart across the line{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>{"\n"}  
      In my face is flashing signs{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      Seek it out and ye shall find{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text>{"\n"}  
      Old, but I'm not that old{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>{"\n"}  
      Young, but I'm not that bold{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>{"\n"}  
      I don't think the world is sold{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      I'm just doing what we're told{"\n"}
      {"\n"}
      [Pre-Chorus] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>{"\n"}      
      I feel something so right{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>                    F </Text>{"\n"}  
      Doing the wrong thing{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>{"\n"}      
      I feel something so wrong{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>                    F </Text>{"\n"}  
      Doing the right thing{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      I could lie, could lie, could lie{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      Everything that kills me makes me feel alive{"\n"}
      {"\n"}
      [Chorus] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text>                     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> E </Text>{"\n"}
      Lately, I've been, I've been losing sleep{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>                                           <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> A </Text>{"\n"}  
      Dreaming about the things that we could be{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}>        C#m </Text>                     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '8',})}> E </Text>{"\n"}
      Said, no more counting dollars{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>                                           <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> A </Text>{"\n"}  
      We'll be counting stars, yeah we'll be counting stars{"\n"}
      {"\n"}
      [Verse 1] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}>                    C#m </Text>{"\n"}  
      I feel the love and I feel it burn{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>{"\n"}  
      Down this river, every turn{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>{"\n"}  
      Hope is a four-letter word{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      Make that money, watch it burn{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text>{"\n"}  
      Old, but I'm not that old{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>{"\n"}  
      Young, but I'm not that bold{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>{"\n"}  
      I don't think the world is sold{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      I'm just doing what we're told{"\n"}
      {"\n"}
      [Pre-Chorus] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>{"\n"}      
      I feel something so right{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>                    F </Text>{"\n"}  
      Doing the wrong thing{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text><Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>              <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>{"\n"}      
      I feel something so wrong{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}>                    F </Text>{"\n"}  
      Doing the right thing{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      I could lie, could lie, could lie{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      Everything that kills me makes me feel alive{"\n"}
      {"\n"}
      [Chorus] {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}> C#m </Text>                     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>{"\n"}
      Lately, I've been, I've been losing sleep{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>                                           <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      Dreaming about the things that we could be{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '15',})}>        C#m </Text>                     <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '4',})}> E </Text>{"\n"}
      Said, no more counting dollars{"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '1',})}> B </Text>                                           <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '0',})}> A </Text>{"\n"}  
      We'll be counting stars, yeah we'll be counting stars{"\n"}

      </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ll: {
    color: 'green' 
  }
})

