import React, {Component} from 'react';
import {Text, ScrollView, StyleSheet} from 'react-native';


export default class LyricsScreen extends Component { 

render() {
    const { navigation } = this.props;

    const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <ScrollView >
      <Text>
      FRESH EYES {"\n"}
      Artist: Andy Grammer{"\n"}
      {"\n"}
      {"\n"}
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text> <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text> <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text> <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text> <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
      {"\n"}
       [Verse]{"\n"}
       {"\n"}
       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>                         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
        Walking down 29th and Park{"\n"}
        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>                        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
         saw you in another’s arms{"\n"}
         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>                       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
         Only a month we’ve been apart{"\n"}
         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}>               Am </Text>                        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                   <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
         You look happier{"\n"}
         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>                         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
        Saw you walk inside a bar{"\n"}
        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>                         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
        He said something to make you laugh{"\n"}
        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>                         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
        I saw that both your smiles were twice as wide as ours{"\n"}
        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}>         Am </Text>                         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
        Yeah, you look happier, you do{"\n"}
        {"\n"}
       [Pre-Chorus]{"\n"}
       {"\n"}
       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>                         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
        Ain’t nobody hurt you like I hurt you{"\n"}
        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}>     Am </Text>                        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
         But ain’t nobody love you like I do{"\n"}
         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>                       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
         Promise that I will not take it personal, baby{"\n"}
         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}> Am </Text>                       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
         If you’re moving on with someone new{"\n"}
         {"\n"}
       [Chorus]{"\n"}
       {"\n"}
       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}>      Am </Text>                         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                    <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
        Cause baby you look happier, you do{"\n"}
        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}>     Am </Text>                        <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                 <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
         My friends told me one day I’d feel it too{"\n"}
         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}>    Am </Text>                       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
         And until then I'll smile to hide the truth{"\n"}
         <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '7',})}>    Am </Text>                       <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '5',})}> F </Text>                      <Text style={[styles.ll]} onPress={() => this.props.navigation.navigate('chord',{chordz: '2',})}> C </Text> {"\n"} 
         But I know I was happier with you{"\n"}
      </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ll: {
    color: 'green' 
  }
})

